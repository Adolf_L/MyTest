import React , {Component} from 'react';
import {FormGroup, Input, Select, ButtonSelect, Toast} from '@welab/xlib-react-components';
import {Steps, Step} from '../component/StepIndex'

import GlobalHelper from '../helper/global';
import Validate from '../helper/validator';

import FullScreen from '../component/FullScreen';
import DateSelect from '../component/DateSelect';
import Location from '../component/Location';

class Profile extends Component{
  constructor(props){
    super(props);
    const pathname = this.props.location.pathname;
    this.state = {
      isInMyData: pathname === '/mydata/profile'
    }
    this.profileFields=['location','family_street','company_name','entry_time','telephone','company_address','company_street','relationship','name','mobile'];
    this.rltships = [{id:'parents',name:'父母'},{id:'spouse',name:'配偶'}];
    this.formValidate=this.formValidate.bind(this);
    this.submit=this.submit.bind(this);
    this.fecthAreas =  this.fecthAreas.bind(this);
  }
  componentDidMount(){
    !this.state.isInMyData && utils.updatePageTitle("身份信息");
    // 查个人资料
    API.get('jddBaseInfo',{platformPath: 'apply-gateway'}).done((res) => {
      let userInfo = res.data;
      // 显示家庭居住信息
      this.refs["location"].setValue(userInfo.profile.residentAddress.province, userInfo.profile.residentAddress.city, userInfo.profile.residentAddress.district);
      this.refs["family_street"].setValue(userInfo.profile.residentAddress.street);
      // 显示公司相关信息
      this.refs["company_name"].setValue(userInfo.company.name);
      if (userInfo.company.entryTime) {
        let entryTimeArr = utils.formatDate(new Date(userInfo.company.entryTime),"yyyy-MM-dd").split('-');
        this.refs["entry_time"].setValue(entryTimeArr[0], entryTimeArr[1]);
      }
      this.refs["telephone"].setValue(userInfo.company.telephone);
      this.refs['company_address'].setValue(userInfo.company.address.province, userInfo.company.address.city, userInfo.company.address.district);
      this.refs["company_street"].setValue(userInfo.company.address.street);
      // 显示联系人相关信息
      userInfo.liaison.relationship && this.refs["relationship"].setValue(userInfo.liaison.relationship);
      this.refs["name"].setValue(userInfo.liaison.name);
      this.refs["mobile"].setValue(userInfo.liaison.mobile);
    }).fail((xhr) => {
      utils.errorHandle(xhr,null,this.toast.show);
    });
  }
  // 初始化地址数据
  fecthAreas(callback){
    GlobalHelper.loadConfigData('areas', callback)
  }
  // 检查表单数据
  formValidate(){
    let validateMsg = '';
    this.profileFields.map((item) => {
      const itemValue = this.refs[item].getValue();
      if (item === 'entry_time') {
        for (let i in itemValue) {
          if(!itemValue[i]){
            validateMsg += '请选择入职时间,';
            break;
          }
        }
      }else{
        if(item != 'telephone' || itemValue){
          let validate = Validate.validate(item, itemValue);
          if (!validate.result) {
            validateMsg += validate.errorMessage + ',';
          }
        }
      }
    });
    return validateMsg.split(',')[0];
  }
  // 提交
  submit(){
    const validateResult = this.formValidate();
    if (validateResult) {
      this.toast.show(validateResult);
      return;
    }
    // 读取用户信息
    let userInfo = {
      'platformPath': 'apply-gateway',
      'company': {'address': {}},
      'profile': {'residentAddress': {}},
      'liaison': {}
    };
    this.profileFields.map((item) => {
      const itemValue = this.refs[item].getValue();
      if (item === 'location') {
        userInfo.profile.residentAddress['province'] = itemValue['province'];
        userInfo.profile.residentAddress['city'] = itemValue['city'];
        userInfo.profile.residentAddress['district'] = itemValue['district'];
      }else if (item === 'family_street') {
        userInfo.profile.residentAddress['street'] = itemValue;
      }else if (item === 'company_name') {
        userInfo.company['name'] = itemValue;
      }else if (item === 'entry_time') {
        userInfo.company['entryTime'] = itemValue['year'] + '-' + itemValue['month'] + '-' + '01';
      }else if (item === 'telephone') {
        userInfo.company['telephone'] = itemValue;
      }else if (item === 'company_address') {
        userInfo.company.address['province']= itemValue['province'];
        userInfo.company.address['city'] = itemValue['city'];
        userInfo.company.address['district'] = itemValue['district'];
      }else if (item === 'company_street') {
        userInfo.company.address['street'] = itemValue;
      }else if (item === 'name' || item === 'mobile' || item === 'relationship') {
        userInfo.liaison[item] = itemValue;
      }
    });
    // 保存用户信息
    API.post('jddBaseInfo', userInfo).done(() => {
      this.props.history.push(this.state.isInMyData ? '/apply' : '/operatorAuth');
    }).fail((xhr) => {
      utils.errorHandle(xhr,null,this.toast.show);
    });
  }
  render(){
    const {isInMyData} = this.state;
    const title = '请补充联系信息，方便发生紧急情况如账号被盗时联系你';
    const styles = !isInMyData ? {'marginTop': ''} : {'marginTop': '0.33rem'};
    return(
      <FullScreen footer={false} className='profile'>
        {
          !isInMyData ? <div className="section">
                          <Steps current={0}>
                            <Step title="身份信息" icon={<i className="iconfont proIcon">&#xe65b;</i>} />
                            <Step title="实名认证" icon={<i className="iconfont proIcon">&#xe65c;</i>} />
                            <Step title="提交申请" icon={<i className="iconfont proIcon">&#xe65a;</i>} />
                          </Steps>
                        </div> : ''
        }

        <div className="tipDesc">{!isInMyData ? title : ''}</div>

        <div className="section" style={{...styles}}>
          <FormGroup withLabel={true} label="居住地址" className="arrowRight">
            <Location ref="location" fecthAreas={ this.fecthAreas} placeholder="请选择省市区" />
          </FormGroup>
          <FormGroup withLabel={true} label="详细地址">
            <Input ref="family_street" attributes={{placeholder:"请填写详细地址",maxLength:50}} />
          </FormGroup>
        </div>

        <div className="section">
          <FormGroup withLabel={true} label="公司名称">
            <Input ref="company_name" attributes={{placeholder:"例:深圳卫盈智信科技有限公司",maxLength:50}} />
          </FormGroup>
          <FormGroup withLabel={true} label="入职时间" className="arrowRight">
            <DateSelect ref="entry_time" placeholder="请选择入职时间"  />
          </FormGroup>
          <FormGroup withLabel={true} label="公司电话">
            <Input ref="telephone" attributes={{placeholder:"(选填) 例:0755-12345678"}} />
          </FormGroup>
          <FormGroup withLabel={true} label="公司地址" className="arrowRight">
            <Location ref="company_address" fecthAreas={this.fecthAreas} placeholder="请选择省市区" />
          </FormGroup>
          <FormGroup withLabel={true} label="详细地址">
            <Input ref="company_street" attributes={{placeholder:"请填写详细地址",maxLength:50}} />
          </FormGroup>
        </div>

        <div className="section">
          <FormGroup withLabel={true} label="关系" className="clearfix">
            <ButtonSelect ref="relationship" defaultValue="parents" options={this.rltships}/>
          </FormGroup>
          <FormGroup withLabel={true} label="联系人" >
            <Input ref="name" attributes={{placeholder:"请填写联系人姓名",maxLength:24}} />
          </FormGroup>
          <FormGroup withLabel={true} label="手机" >
            <Input ref="mobile" attributes={{placeholder:"请填写联系人手机", maxLength: 11}} />
          </FormGroup>
        </div>
        <div className="actions">
          <button className="btn" onClick={ this.submit }>{!isInMyData ? '下一步' : '保存'}</button>
        </div>
        <Toast ref={(toast) => this.toast = toast} timeout={1500}/>
      </FullScreen>
    )
  }
}
export default Profile;
