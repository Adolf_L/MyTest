import React , {Component} from 'react';
import Config from '../helper/config';
import Bizhelper from '../helper/biz';
import FullScreen from '../component/FullScreen';
import {ImageUploader, Toast} from '@welab/xlib-react-components';

class IDCard extends Component{
  constructor(props){
    super(props);
    const pathname = this.props.location.pathname;
    const idInfo = store.session('idInfo');
    this.state = {
      isInMyData: pathname === '/mydata/idcard',
      checked: store.session('idInfo'),
      idCards: [
        {
          type: 'front',
          docType: 'id_front_proof',
          isUpload: idInfo && idInfo.name&& idInfo.cnid,
          imgUrl: '',
          exampleUrl: '//web.wolaidai.com/img/jddv3/id_card_front.png',
          loadUrl: '//web.wolaidai.com/img/jddv3/id_upload.png'
        },{
          type: 'back',
          docType: 'id_back_proof',
          isUpload: idInfo && idInfo.validPeriod,
          imgUrl: '',
          exampleUrl: '//web.wolaidai.com/img/jddv3/id_card_back.png',
          loadUrl: '//web.wolaidai.com/img/jddv3/id_upload.png'
        }
      ],
      idInfo:{
        ...idInfo
      }
    }
  }

  componentDidMount(){
    !this.state.isInMyData && utils.updatePageTitle("身份信息");
    API.get('v3/identification',{ platformPath: 'welab-ocr'}).done((data) => {
      console.log(data);
    }).fail((xhr) => {
      utils.errorHandle(xhr, '',this.toast.show);
    })
    

  }

  formatValidPeriod(timeLimit) {
    const timeArr = timeLimit.split('-');
    timeArr.map((item,index) => {
      item = [].slice.apply(item);
      item.splice(4,0,'.');
      item.splice(7,0,'.');
      item = item.join('');
      timeArr[index] = item;
    })
    return timeArr.join('-');
  }

  uploadPhoto(type,docType) {
    return (file) => {
      const { idCards, idInfo } = this.state;
      const params = {
        platformPath: 'welab-ocr',
        base64Data: file,
        side: type,
        vendor: 'linkface'
      }
      API.upload('v3/ocr/idcard', params).done(data => {
        idCards.some((item,index) => {
        item.type === type && (item.imgUrl = file) && (item.isUpload = true)
      })
        const info = data.data[type];
        switch (type) {
          case 'front':
            idInfo.name = info.name;
            idInfo.cnid = info.cnid;
            break;
          case 'back':
            idInfo.validPeriod = this.formatValidPeriod(info.timeLimit);
            break;
          default:
            break;
        }
        this.setState({
          checked: true,
          idCards: idCards,
          idInfo: idInfo
        },() => {
          store.session('idInfo',idInfo);
        })
      }).fail((xhr) => {
        utils.errorHandle(xhr, '',this.toast.show);
      });

    }
  }

  submit() {
    const { idInfo } = this.state;
    if(idInfo.name && idInfo.cnid && idInfo.validPeriod) {
      this.props.history.push('zmxyAuth');
    } else {
      this.toast.show('您的信息未完善，请检查您的身份证正反面照片是否正常上传');
    }

  }

  render(){
    const { idCards,idInfo,checked,isInMyData } = this.state;
    let idInfoReady  = true;
    for(let i in idInfo) {
      !idInfo[i] && (idInfoReady = false);
    }
    const title = idInfoReady ? '请核对信息，如有误请重拍，提交后不可更改' : '请拍摄您的二代身份证，自动智能识别';
    const styles = isInMyData ? {'color':'#CCCCCC'} : {'color': '#333'};
    return(
      <FullScreen className='id-card' footer={false}>
        <Toast ref={t => this.toast = t}></Toast>
        <div className='ocr-div'>
          <h1>{!isInMyData ? title : ''}</h1>
          <div className='images'>
            {
              idCards.map((item,index) => {
                return (
                  <ImageUploader key={ index } 
                  imgURI={ item.isUpload ? `${item.loadUrl}?` + new Date().getTime() : item.exampleUrl }
                  onProcessDone= { this.uploadPhoto.call(this, item.type, item.docType) }>  
                  </ImageUploader>
                )
              })
            }
          </div>
        </div>
        {
          checked ? 
          <div className='info-div'>
            <div>
              <div>姓名</div>
              <div style={{...styles}}>{ idInfo.name }</div>
            </div>
            <div>
              <div>身份号码</div>
              <div style={{...styles}}>{ idInfo.cnid }</div>
            </div>
            <div>
              <div>有效期限</div>
              <div style={{...styles}}>{ idInfo.validPeriod }</div>
            </div>
          </div> : null 
        }
        {!isInMyData ? <button className="btn" onClick={ this.submit.bind(this) }>提交</button> : 
        <p className="bottom" >2017-09-07实名认证通过，如需修改，请联系客服</p> }
      </FullScreen>

    )
  }
}
export default IDCard;