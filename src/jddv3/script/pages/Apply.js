import React from 'react';
import Installment from '../component/Installment';
import {Toast} from '@welab/xlib-react-components';
import FontIcon from '../component/FontIcon';
import Biz from '../helper/biz';
import FullScreen from "../component/FullScreen";

const MaxLoanLimit = {
  offer: 30000,
  jdd: 30000//本来是正常五万要授权人行征信，但是因为转化率大幅下降，所以暂调回3w
};

export default class Apply extends React.Component {
  constructor(props) {
    super(props);
    this.isCreditAuth = false;
    this.state = {
      loanLimit: this.getMaxLoanLimit(),
      profileChecked: false,
      authChecked: false
    };
  }

  getMaxLoanLimit() {
    return MaxLoanLimit['jdd'];
  }

  onSubmit() {
    if (!this.refs['inst'].doValidate()) {
      return false;
    }
    Biz.applyUserCheck((checked) => {
      if (checked) {
        let instValue = this.refs['inst'].getValue();
        $.fn.cookie('amount', instValue.amount);
        $.fn.cookie('tenor', instValue.tenor);
        if (this.state.isNewUser) {
          this.props.history.push('idcard');
        } else {
          this.props.history.push('submitApply');
        }
      }
    }, this.toast.show);
  }

  onInstLoaded() {
    let amount = $.fn.cookie('amount'), tenor = $.fn.cookie('tenor');
    if (amount && tenor) {
      this.refs['inst'].setValue(amount - 0, tenor);
    }
  }

  pageChange(params) {
    this.props.history.push(params);
  }

  componentDidMount() {
    utils.updatePageTitle('简单贷');
    const getUserProfile = new Promise((resolve,reject) => {
      API.get('jddProfile', {platformPath: 'apply-gateway'}).done((res) => {
        resolve(res);
      }).fail((xhr) => {
        reject(xhr);
      });
    });
    const judgeUser = new Promise((resolve,reject) => {
      API.get('h5/jddv3/user').done((res) => {
        resolve(res);
      }).fail((xhr) => {
        reject(xhr);
      });
    });
    Promise.all([getUserProfile,judgeUser]).then((data) => {
      this.setState({
        userName:data[0].data.name,
        isNewUser: data[1].noLoan
      })
    },(xhr) => {
      utils.errorHandle(xhr, '', this.toast.show);
    })
  }

  render() {
    const phone = $.fn.cookie('X-User-Mobile');
    const isNewUser = this.state.isNewUser;
    var str;
    if (str) {
      str = '您有一笔***元贷款正在审核中，点击查看';
    } else if (str) {
      str = '您有一笔***元贷款已审核通过，请立即确认放款';
    } else {
      str = '您有一笔***元贷款被退回，请补充资料后重新提交审核';
    }
    return (
      <FullScreen>
        <Toast ref={t => this.toast = t}></Toast>
        <div className="jdd-apply">
          <div className="jdd-apply-banner">
            <div className={isNewUser ? 'oldUser bannerTips' : 'oldUser bannerTips'}>
              <span>{str}</span>
            </div>
            <div className={isNewUser ? 'active newUser' : 'newUser'}>
              <p className="title">最高可申请(元)</p>
              <p className="amount">{this.state.loanLimit}</p>
              <p className="guider">请准备好身份证，2分钟可完成申请</p>
            </div>
            <div className={isNewUser ? 'oldUser' : 'oldUser active'}>
              <img className="borrower-headPortrait" src="//web.wolaidai.com/img/jddv3/headPortrait-default.png"
                   alt=""/>
              <div className="borrower-wrap"><span className="borrower-phone">{this.state.userName}</span> <span
                className="borrower-name">{phone}</span></div>
            </div>
          </div>
          <div className={isNewUser ? 'oldUser intro-card-float' : 'oldUser intro-card-float active'}>
            <a className="card card-myInfo" onClick={this.pageChange.bind(this, 'mydata/idcard')}>
              <FontIcon iconName="ziliao" fontSize='48px'/>
              <div>我的资料</div>
            </a>
            <a className="card card-borrowRecord" onClick={this.pageChange.bind(this, '')}>
              <FontIcon iconName="daikuan" fontSize='48px'/>
              <div>借款记录</div>
            </a>
          </div>
          <Installment ref="inst" maxValue={this.state.loanLimit}
                       onDataLoaded={this.onInstLoaded.bind(this)}/>
          <div className="btn-box">
            <button className="btn" onClick={this.onSubmit.bind(this)}>立即申请</button>
          </div>
        </div>
      </FullScreen>
    );
  }
}