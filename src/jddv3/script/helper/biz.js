import Config from './config';
export default class Biz{
  static applyAmountCheck(amount,callback){
    API.get('v3/user/infos_and_application_allowed?amount='+amount).done((res)=>{
      callback(true);
    }).fail((xhr)=>{
      callback(false,xhr);
    });
  }
  static applyUserCheck(callback,toast){
    //老用户判断下身份，屏蔽学生贷款
    if (!$.fn.cookie('IS_NEW_USER')) {
      API.get('v3/user/login_info').done((res)=>{
        if (res.roleType===1) {
          toast('抱歉，暂不支持学生用户哦');
          callback(false);
        }else{
          callback(true);
        }
      }).fail((xhr)=>{
        utils.errorHandle(xhr,'',toast);
        callback(false);
      });
    } else {
      callback(true);
    }
  }
  static switchPageInfo(callback){
    function originSchema() {
      API.get('v4/partner/user/switch_page_info').done((data)=>{
        let res = data.switch_page;
        //none:没有贷款, on_road:在途贷款, repayment:贷款还款中, reject:贷款被拒绝, confirmed: 贷款确认
        switch(res){
          case 'none':
            callback('apply');
            break;
          case 'on_road':
            if (data.loan_application_state == 'push_backed') {
              callback(`apply/push_back/${data.application_id}`);
            }else{
              callback('apply/auditting');
            }
            break;
          case 'repayment':
            callback('apply/repayment');
            break;
          case 'reject':
            callback(`apply/reject/${data.application_id}`);
            break;
          case 'confirmed':
            callback(`apply/confirm/${data.application_id}`);
            break;
          default:
            callback('apply');
            break;
        }
      }).fail((xhr, errorType, error)=>{
        utils.errorHandle(xhr, error);
        callback('apply');
      });
    }

    originSchema();

  }

  static goStaff(staffPath) {
    if (!utils.isLogin()) {
      return;
    }
    if (!utils.isProductEnv()){
      $.fn.cookie('wolaidai_api_path', store.session('apiDomain')+'v2/', {path:'/'});
    }
    $.fn.cookie('H5-Product-Code',Config.app.productCode, {path:'/staff'});
    location.href = `/staff/main.html#/${staffPath}`;
  }

  // 保存身份信息
  static cacheIdInfo(idInfo){
    store.session('idInfo', idInfo);
  }

  // 获取身份信息
  static fetchIdInfo(){
    return store.session('idInfo');
  }

  static getUserBaseInfo(cb, alertMsg=true,toast){
    const idInfo = this.fetchIdInfo();
    // 若session中已经存入用户身份信息,说明已经进行了OCR,调用callback
    if(idInfo) {
      cb && typeof cb === "function" && cb(idInfo);
      return;
    }

    const params = {platformPath: 'apply-gateway'};
    // 身份信息查询,获取用户姓名,身份证号
    API.get('jddProfile',params).done((result) => {
      if(Object.keys(result.data).length == 0){
        // 没有进行身份证扫描
        alertMsg && toast('亲，请先扫描身份证~');
        return;
      }else{
        // 保存用户身份信息
        this.cacheIdInfo(result.data);
      }
      if(cb && typeof(cb)==="function"){
        cb(result.data);
      }
    }).fail((xhr) => {
      utils.errorHandle(xhr,'',toast);
    })
  }
}