## Installation

`(recommend)` Install [Yarn](https://yarnpkg.com/) instead of NPM

```sh
$ yarn install
```

## Quick Start

run the app:

```bash
$ yarn run server
```

build the app:

```bash
$ yarn run build
```
